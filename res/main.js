const app = {
    init() {
        $('.start').on('click', () => {
            $('.intro-layer').fadeOut();

            setTimeout(() => this.goTo('start'), 1000)
        });

        $(document).on('click', '[data-step]', (e) => {
            this.goTo($(e.currentTarget).data('step'), $(e.currentTarget));
        });
    },

    /**
     * Go to page
     * @param  {string} uri     Page URI
     * @param  {object} $sender Button that fired the event
     */
    goTo(uri, $sender) {
        $.get(`story/${uri}.json5`)
        .done((json5) => {
            this.showPage(JSON5.parse(json5));
        })
        .fail(() => {
            $sender.prop('disabled', true);
            $sender.append('<span class="error">Impossible de charger l’histoire.</span>');
        });
    },

    /**
     * Render page
     * @param  {object} page Page object
     */
    showPage(page) {
        $('main').html(`
            <h2>${page.title}</h2>
            ${this.renderContent(page.content)}

            <div class="actions">
                ${this.renderActions(page.actions)}
            </div>
        `);

        this.showBackground(page.background);
    },

    renderActions(actions) {
        let html = '';

        actions.forEach((action) => {
            html += `<button data-step="${action.to}">${action.title}</button>`;
        });

        return html;
    },

    renderContent(content) {
        return content.replace(/([^\n]*)/g, '<p>$1</p>')
            .replace(/\*([^\b]+)\*/, '<strong>$1</strong>')
            .replace(/\_([^\b]+)\_/, '<em>$1</em>');
    },

    showBackground(background) {
        $oldBackgrounds = $('.background');

        let style;

        if (background.type === 'color') {
            style = `background-color: ${background.name}`;
        } else if (background.type === 'img') {
            style = `background-image: url(img/${background.name})`;
        }

        $(`<div class="background" style="${style}"></div>`)
            .appendTo($('body'));

        setTimeout(() => $oldBackgrounds.remove(), 1111);
    },
};

app.init();
